#ifndef CMAIN_H
#define CMAIN_H

#include "SDL.h"
#include "SDL_image.h"
#include "defs.h"
#include "SDL_mixer.h"
#include "SDL_ttf.h"

class CMain
{
    public:
        CMain(bool *isRunning,const char *name="Template SDL2",int w=SCREEN_W,int h=SCREEN_H,int posX=SDL_WINDOWPOS_CENTERED,int posY=SDL_WINDOWPOS_CENTERED);
        ~CMain();
        SDL_Renderer* getRenderer(void){return renderer;};
        void Cbegin(void);
        void Cend(void);
        SDL_Window* getWindow(void) const {return window;};


    protected:
        SDL_Window  *window;
        SDL_Renderer *renderer;
};

#endif // CMAIN_H
