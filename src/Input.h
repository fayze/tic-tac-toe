#ifndef INPUT_H
#define INPUT_H
#include "SDL.h"

class Input
{
    public:
        Input();
        ~Input();

        void getInputs(SDL_Event *ev);
        bool mouseLeftB(void) const {    return m_mouseLeft;     }
        bool mouseRightB(void) const {   return m_mouseRight;    }
        bool escapeB(void) const {   return m_escape;    }
        bool returnB(void) const {   return m_return;    }
        void reset(void);
        bool eventHappened(void) const;

    protected:

    private:
        bool m_mouseLeft;
        bool m_mouseRight;
        bool m_escape;
        bool m_return;
};

#endif // INPUT_H
