#include "Input.h"

Input::Input()
{
    m_mouseLeft=false;
    m_mouseRight=false;
    m_escape=false;
    m_return=false;
}

void Input::getInputs(SDL_Event *ev)
{
    reset();
    while(SDL_PollEvent(ev)!=0)
    {
        const Uint8 *key=SDL_GetKeyboardState(NULL);
        switch(ev->type)
        {
            case SDL_MOUSEBUTTONDOWN:
                if(ev->button.button==SDL_BUTTON_LEFT)
                    m_mouseLeft=true;
                if(ev->button.button==SDL_BUTTON_RIGHT)
                    m_mouseRight=true;
            break;

            case SDL_MOUSEBUTTONUP:
                if(ev->button.button==SDL_BUTTON_LEFT)
                    m_mouseLeft=false;
                if(ev->button.button==SDL_BUTTON_RIGHT)
                    m_mouseRight=false;
            break;

            default:
                break;
        }

        if(key[SDL_SCANCODE_ESCAPE])
            m_escape=true;
        if(key[SDL_SCANCODE_RETURN])
            m_return=true;
    }
}

bool Input::eventHappened() const
{
    bool state=false;

    if(m_mouseLeft || m_mouseRight || m_escape || m_return)
        state=true;

    return state;
}

void Input::reset(void)
{
    m_mouseLeft=false;
    m_mouseRight=false;
    m_escape=false;
    m_return=false;
}


Input::~Input()
{
    //dtor
}
