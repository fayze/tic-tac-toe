#include "Game.h"

using namespace std;

Game::Game():CMain(&isRunning,"Tic Tac Toe",320,430)
{
    //ctor
    action=false;
    mouseX=0;
    mouseY=0;
    turn=Etat::circle;
    m_cercleInt=0;
    m_croixInt=0;

    m_backGround=new Image(this->renderer,"./pics/background.png",&isRunning);

    m_cercle=new Image(this->renderer,"./pics/cercle.png",&isRunning);

    m_croix=new Image(this->renderer,"./pics/croix.png",&isRunning);

    m_reset= new Text(this->renderer,&isRunning,"Restart",20,1);
    m_reset->setFont("./fonts/SHOWG.ttf",20);
    m_reset->setXY(120,370);
    m_reset->setBackGroundColor(0,185,185,255);

    m_scoreX =new Text(this->renderer,&isRunning,"0");
    m_scoreX->setFont("./fonts/SHOWG.ttf",30);
    m_scoreX->setXY(290,370);

    m_scoreO =new Text(this->renderer,&isRunning,"0");
    m_scoreO->setFont("./fonts/SHOWG.ttf",30);
    m_scoreO->setXY(15,370);

    m_result=new Text(this->renderer,&isRunning,"X a gagn�",70);
    m_result->setXY(15,0);
    m_result->setTextColor(255,255,255,255);
    m_result->setFont("./fonts/blackr.ttf",70);

    m_sound = new Music("./mix/bip.wav",&isRunning,true);

    m_input = new Input();


    for(int i=0;i<9;i++)
        tab.push_back(Etat::vide);

    Cbegin();
    draw(-1);
    Cend();
}

void Game::mainLoop(void)
{
    while(isRunning)
    {
        m_input->getInputs(&ev);
        isRunning=!((ev.type==SDL_QUIT)||(m_input->escapeB()));
        SDL_GetMouseState(&mouseX,&mouseY);
        int a=-1;
        if(proccessEvents())
        {
            fillTab();
            a = checkGameState();


            Cend();
        }
        draw(a);
        SDL_Delay(50);
    }
}

void Game::drawRectAt(int x,int y,bool vertical) const
{
    int w=0,h=0;
    if(vertical)
    {
        w=10;
        h=320;
    }
    if(!vertical)
    {
        w=320;
        h=10;
    }
    SDL_Rect rect={x,y,w,h};
    SDL_RenderFillRect(this->renderer,&rect);
}

void Game::drawGrid(void) const
{
     //horizontal
    drawRectAt(0,100,false);
    drawRectAt(0,210,false);
    SDL_Rect rect={0,320,320,10};
    SDL_RenderFillRect(this->renderer,&rect);
    //vertical
    drawRectAt(100,0,true);
    drawRectAt(210,0,true);
}

void Game::drawResult(int a)
{
    if(a==static_cast<int>(Etat::circle))
    {
        m_result->loadTextureFromText("O a gagn�!!");
    }
    else if(a==static_cast<int>(Etat::cross))
    {
        m_result->loadTextureFromText("X a gagn�!!");
    }
    else if(a==0)
    {
        m_result->loadTextureFromText("Egalit�!!");
    }

    m_result->draw();
}

void Game::draw(int a)
{
    Cbegin();
    m_backGround->draw();

    drawGrid();
    for(int i=0;i<3;i++)
    {
        for(int j=0;j<3;j++)
        {
            if(tab[j+i*3]==Etat::circle)
                m_cercle->drawAt(j*110,i*110);
            else if(tab[j+i*3]==Etat::cross)
                m_croix->drawAt(j*110,i*110);
        }
    }
    m_reset->activateMouseOverEffect();
    m_reset->draw();
    m_scoreO->changeNumber(m_cercleInt);
    m_scoreX->changeNumber(m_croixInt);
    m_scoreO->draw();
    m_scoreX->draw();
    m_cercle->drawAt(13,340,20,20);
    m_croix->drawAt(288,340,20,20);
    Cend();


    if(a!=-1)
    {
        drawResult(a);
        Cend();
        do
        {
            m_input->getInputs(&ev);
            SDL_Delay(50);
        }while(!m_input->eventHappened());
    }
}

bool Game::proccessEvents(void)
{
    bool goodEvent=false;
    if(m_input->eventHappened())
    {
        if(((mouseX>=0 && mouseX<=100) || (mouseX>=110 && mouseX<=210) || (mouseX>=220 && mouseX<=320)) &&
           ((mouseY>=0 && mouseY<=100) || (mouseY>=110 && mouseY<=210) || (mouseY>=220 && mouseY<=320)))
        {
                goodEvent=true;
                m_sound->playChunk();
        }
        if(m_reset->isMouseOn())
            goodEvent=true;;
    }
    return goodEvent;
}

void Game::fillTab(void)
{
    std::vector<Etat> copie;
    copie=tab;
    int pos=0,i=0,j=0;

    bool found=false;
    for(int yMin=0, yMax=100;yMin<=220;yMin+=110,yMax+=110)
    {
        j=0;
        for(int xMin=0, xMax=100;xMin<=220;xMin+=110,xMax+=110)
        {
            if((mouseX>=xMin && mouseX<=xMax) && (mouseY>=yMin && mouseY<=yMax))
            {
                pos=j+i*3;
                found=true;
                break;
            }
            j++;
        }
        i++;
    }
    if(tab[pos]==Etat::vide && found)
    {
        tab[pos]=turn;
        if(copie!=tab)
        {   turn=(turn==Etat::circle)?Etat::cross : Etat::circle;   }
    }
}

int Game::logic(void)
{
    Etat state=Etat::vide;
    if(tab[0]!=Etat::vide)
    {
        if((tab[0]==tab[1] && tab[0]==tab[2]) || (tab[0]==tab[3] && tab[0]==tab[6])
            || (tab[0]==tab[4] && tab[0]==tab[8]))
            state=tab[0];
    }
    if(tab[6]!=Etat::vide)
    {
       if((tab[6]==tab[7] && tab[6]==tab[8]) || (tab[6]==tab[4] && tab[6]==tab[2]))
            state=tab[6];
    }
    if(tab[5]!=Etat::vide)
    {
       if((tab[5]==tab[4] && tab[5]==tab[3]) || (tab[2]==tab[5] && tab[5]==tab[8]))
            state=tab[5];
    }
    if(tab[1]!=Etat::vide)
    {
       if(tab[1]==tab[4] && tab[1]==tab[7])
            state=tab[1];
    }

    return static_cast<int>(state);
}

void Game::reset(void)
{
    m_cercleInt=0;
    m_croixInt=0;
    turn=Etat::circle;

    for(int i=0;i<9;i++)
        tab[i]=(Etat::vide);
}

void Game::restart(void)
{
    turn=Etat::circle;

    for(int i=0;i<9;i++)
        tab[i]=(Etat::vide);
}

bool Game::isNull()
{
    bool finished=true;
    for(auto a: tab)
    {
        if(a==Etat::vide)
            finished=false;
    }
    if(finished)
        restart();
    return finished;
}

int Game::checkGameState(void)
{
    int state=logic();
    if(state!=0)
    {
        if(logic()==static_cast<int>(Etat::circle))
            m_cercleInt++;
        if(logic()==static_cast<int>(Etat::cross))
            m_croixInt++;
        restart();
    }
    else
    {
        if(!isNull())
        {
            state=-1;
        }
    }
    if(m_reset->isMouseOn())
        reset();

    return state;
}

Game::~Game()
{
    delete m_backGround;
    delete m_cercle;
    delete m_croix;
    delete m_reset;
    delete m_scoreO;
    delete m_scoreX;
    delete m_sound;
    delete m_input;
    delete m_result;

}
