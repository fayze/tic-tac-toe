#include "Image.h"
#include <iostream>

using namespace std;

Image::Image(SDL_Renderer *renderer,const char *fichier,bool *isRunning)
{

    color={185,185,185,0};

    selectionState=false;
    buttonMode=true;

    m_image=NULL;
    m_renderer=NULL;
    m_renderer=renderer;

    m_posImage.x=0;
    m_posImage.y=0;
    m_posImage.w=0;
    m_posImage.h=0;
    textureW=0;
    textureH=0;

    SDL_Surface *surf=NULL;
    surf=IMG_Load(fichier);

    if(surf==NULL)
        cout <<"Impossible de charger l'image "<<IMG_GetError();


    if(m_renderer==NULL)
    {
        cout <<"Impossible erreur de passage de renderer "<<SDL_GetError();
        *isRunning=false;
    }

    m_image=SDL_CreateTextureFromSurface(m_renderer,surf);

    if(m_image==NULL)
    {
        cout <<"Impossible de charger l'image "<<SDL_GetError();
        *isRunning=false;
    }
    SDL_QueryTexture(m_image,NULL,NULL,&textureW,&textureH);
    m_posImage.w=textureW;
    m_posImage.h=textureH;

    //ctor
}

void Image::draw(void){
    SDL_RenderCopy(m_renderer,m_image,NULL,&m_posImage);
}

void Image::drawAt(int x,int y,int w,int h){
    SDL_Rect rect={x,y,w,h};

    if(w==0 && h==0)
        SDL_QueryTexture(m_image,NULL,NULL,&rect.w,&rect.h);
    SDL_RenderCopy(m_renderer,m_image,NULL,&rect);
}

void Image::setRect(int x,int y,int w,int h){
    m_posImage.x=x;
    m_posImage.y=y;
    m_posImage.w=w;
    m_posImage.h=h;
}

void Image::setRect(SDL_Rect rect){
    m_posImage.x=rect.x;
    m_posImage.y=rect.y;
    m_posImage.w=rect.w;
    m_posImage.h=rect.h;
}

void Image::setX(int x){
m_posImage.x=x;
}

void Image::changeTexture(const char* fichier,SDL_Renderer *render)
{
    SDL_DestroyTexture(m_image);
    m_image=NULL;
    SDL_Surface *surf=NULL;
    surf=IMG_Load(fichier);

    if(surf==NULL)
        cout <<"Impossible de charger l'image "<<IMG_GetError();

    m_image=SDL_CreateTextureFromSurface(render,surf);
    m_renderer=render;

     if(m_image==NULL)
    {
        cout <<"Impossible de charger l'image "<<SDL_GetError();
    }
    SDL_QueryTexture(m_image,NULL,NULL,&textureW,&textureH);
    m_posImage.w=textureW;
    m_posImage.h=textureH;

}

bool Image::isMouseOn(void) const
{
    bool state=false;
    int mouseX=0,mouseY=0;

    SDL_GetMouseState(&mouseX,&mouseY);

    if(buttonMode)
        if( (mouseX>=m_posImage.x && mouseX<=m_posImage.x+m_posImage.w) &&
        (mouseY>=m_posImage.y && mouseY<=m_posImage.y+m_posImage.h) && selectionState==false)
        state = true;


    return state;

}

void Image::activateMouseOverEffect(void)
{
    bool isIn=false;

    isIn=isMouseOn();


    if(isIn)
    {
        if(SDL_SetTextureColorMod(m_image,color.r,color.g,color.b)<0)
            cout<<SDL_GetError()<<endl;
    }
    else
    {
        if(SDL_SetTextureColorMod(m_image,255,255,255)<0)
            cout<<SDL_GetError()<<endl;
    }
}

void Image::setY(int y){
m_posImage.y=y;
}

void Image::setXY(int x,int y){
m_posImage.x=x;
m_posImage.y=y;
}

void Image::setColorMod(int r,int g,int b,int a)
{
    color.r=r;
    color.g=g;
    color.b=b;
    color.a=a;
}

void Image::setAlphaMod(int a)
{
    a=(a>=0 && a<=255)?a:0;
    if(SDL_SetTextureAlphaMod(m_image,a)<0)
        cout<<"ERROR: "<<SDL_GetError()<<endl;
}

bool Image::isSelected(const SDL_Event &ev)
{

    if(buttonMode)
    {
        if(Image::isMouseOn() && selectionState!=true)
        {
            if(ev.type==SDL_MOUSEBUTTONDOWN)
            {
                if(ev.button.button==SDL_BUTTON_LEFT)
                {
                    selectionState=true;
                    buttonMode=false;
                }
            }
        }
    }
    return selectionState;
}

void Image::unSelect(void)
{
    selectionState=false;
    buttonMode=true;
}

Image::~Image(void)
{
    m_image=NULL;
    m_renderer=NULL;
    //dtor
}
