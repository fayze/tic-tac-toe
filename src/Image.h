#ifndef IMAGE_H
#define IMAGE_H
#include "SDL.h"
#include "SDL_image.h"


class Image
{
    public:
        Image(SDL_Renderer *renderer,const char *fichier,bool *isRunning);
        ~Image(void);
        void setRect(int x,int y,int w,int h);
        void setRect(SDL_Rect rect);
        void draw(void);
        void drawAt(int X,int Y,int w=0,int h=0);
        void setX(int x);
        void setY(int y);
        void setXY(int x,int y);
        void setColorMod(int r,int g,int b,int a);
        void setAlphaMod(int a);
        void unSelect(void);

        void changeTexture(const char *fichier,SDL_Renderer *render);
        void activateMouseOverEffect(void);
        void desactivateButtonMode(void){buttonMode=false;};

        int getX(void) const{return m_posImage.x;};
        int getY(void) const{return m_posImage.y;};
        int getRectW(void) const{return m_posImage.w;};
        int getRectH(void) const{return m_posImage.h;};
        SDL_Texture* getTexture(void) const{return m_image;};
        int getTextureW(void) const{return textureW;};
        int getTextureH(void) const{return textureH;};
        SDL_Rect getRect(void)const{return m_posImage;};
        bool isMouseOn(void)const;
        bool isSelected(const SDL_Event &ev);





    private:
        SDL_Texture *m_image;
        SDL_Rect m_posImage;
        SDL_Renderer *m_renderer;
        SDL_Color color;
        int textureW;
        int textureH;
        bool selectionState;
        bool buttonMode;
};

#endif // IMAGE_H
