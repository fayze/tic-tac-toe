#include "Music.h"
#include <iostream>

Music::Music(const char* filepath,bool *isRunning,bool isChunk,int frequency,int channel,int chunksize):timer(0),position(0),ecart(0),jumpSec(10),isChunK(isChunk)
{

    action=STOP;
    channel=(channel>=0 && channel<=2)?channel:MIX_DEFAULT_CHANNELS;
    if(Mix_OpenAudio(frequency,MIX_DEFAULT_FORMAT,channel,chunksize)<0)
        std::cout <<"ERROR (initializing audio): "<<Mix_GetError()<<std::endl;
    if(!isChunk)
    {
        music=NULL;
        music=Mix_LoadMUS(filepath);
        if(music==NULL)
        {
            std::cout <<"ERROR (loading music): "<<Mix_GetError()<<std::endl;
            *isRunning=false;
        }

    }
    else
    {
        chunk=NULL;

        if(Mix_AllocateChannels(32)==0) //Allouer 32 canaux
        {
            std::cout<<"Error allocating channels: "<<Mix_GetError()<<std::endl;
            *isRunning=false;
        }


        chunk=Mix_LoadWAV(filepath);

        if(chunk==NULL)
        {
            std::cout <<"ERROR (loading chunk): "<<Mix_GetError()<<std::endl;
            *isRunning=false;
        }



        Mix_Volume(1, MIX_MAX_VOLUME); //Mettre � mi-volume le post 1
        Mix_VolumeChunk(chunk, MIX_MAX_VOLUME);


    }

}


int Music::getPosition(void)
{
    if(playingMusic() && !pausedMusic())
    {
        position=(SDL_GetTicks()-timer)/1000;
        position=(position>=0 && timer!=0)?position:0;
    }
    return position;
}

unsigned Music::lenght(void)
{
    playMusic(0);
    unsigned temp=0;
    short increment=10;
    unsigned temps=SDL_GetTicks();

        haltMusic();
        playMusic();
        setMusicPosition(temp);
        while(Mix_PlayingMusic()==1)
        {

            moveForward(increment);
            temp=getPosition();
            std::cout<<"--"<<temp<<"---"<<std::endl;
            if(SDL_GetTicks()-temps>=1000)
            {
                moveBack(1);
                temps=SDL_GetTicks();
            }
            SDL_Delay(20);
            temp=getPosition();
        }


    std::cout<<temp<<std::endl;

    Mix_RewindMusic();
    haltMusic();
    return temp;
}

bool Music::isFinished(void)
{
    if(!Mix_PlayingMusic() && action==PLAYING)
        return true;
    else
        return false;
}

int Music::playMusic(int loops)
{
    action=PLAYING;
    loops=(loops>=-1)?loops:0;
    timer=SDL_GetTicks();
    return Mix_PlayMusic(music,loops);
}

int Music::playChunk(int loops)
{
    loops=(loops>=-1)?loops:0;
    return Mix_PlayChannel(1,chunk,loops);
}

void Music::pauseMusic(void)
{
    action=PAUSE;
    ecart=SDL_GetTicks()-timer;
    getPosition();
    Mix_PauseMusic();
}

void Music::resumeMusic(void)
{
    action=PLAYING;
    timer=SDL_GetTicks()-ecart;
    Mix_ResumeMusic();
}

int Music::haltMusic(void)
{
    action=STOP;
    timer=0;
    position=0;
    ecart=0;

    return Mix_HaltMusic();
}

int Music::setMusicPosition(int seconds)
{
    seconds=(seconds>=0)?seconds:0;
    if(seconds>=0)
        return Mix_SetMusicPosition(seconds);
    else
    {
        std::cout<<"ERROR: Seconds are less than 0. No change! "<<Mix_GetError()<<std::endl;
        return -1;
    }
}

void Music::setJumpTime(int time)
{
    jumpSec=(time>0)?time:10;
}

int Music::moveForward(int time)
{
    if(time!=jumpSec)
        setJumpTime(time);
    getPosition();
    //mix_setmusi
    if(Mix_SetMusicPosition(position+jumpSec)<0)
    {
            std::cout<<"ERROR : cannot make a jump: "<<Mix_GetError()<<std::endl;
            return -1;
    }
    else
        timer-=jumpSec*1000;

    return 1;
}

int Music::moveBack(int time)
{
    int sous=0;
    setJumpTime(time);
    getPosition();
    sous=position-jumpSec;
    if(Mix_SetMusicPosition(((sous>=0)?sous:0))<0)
    {
            std::cout<<"ERROR (cannot make a jump) :"<<Mix_GetError()<<std::endl;
            return -1;
    }
    else if(sous<0)
    {
        std::cout<<"ERROR (cannot make a jump): "<<Mix_GetError()<<std::endl;
        return -1;
    }
    else
        timer+=jumpSec*1000;

    return 1;
}

int Music::changeMusic(const char *filepath)
{
    haltMusic();

    if(music!=NULL)
    {
        Mix_FreeMusic(music);
        music=NULL;
    }
    music=Mix_LoadMUS(filepath);
    if(music==NULL)
    {
        std::cout <<"ERROR (loading music): "<<Mix_GetError()<<std::endl;
        return -1;
    }
    else
    {
    timer=0;
    position=0;
    ecart=0;
    }

    return 1;
}

Music::~Music(void)
{
   Mix_HaltChannel(-1);
    if(!isChunK)
    {
        Mix_FreeMusic(music);
        music=NULL;
    }
    else
    {
        Mix_FreeChunk(chunk);
        chunk=NULL;
    }
}


