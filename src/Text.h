
#ifndef Text_H
#define Text_H
#include "SDL.h"
#include "SDL_ttf.h"
#include "defs.h" // Screen_w, screen_h
#ifdef IMAGE_H
#include "Image.h"
#endif // IMAGE_H

#include <iostream>
#include <sstream>

const std::string DEFAULTFONT="./fonts/sans_serif.ttf";

template <typename T>
std::string to_string(T val)
{
    std::stringstream stream;
    stream << val;
    return stream.str();
}

class Text
{
    public:
        Text(SDL_Renderer *renderer,bool *isRunning,std::string Texte="This is a sample text",int fontSize=24,int ActivateTextBackground=0);
        ~Text(void);
        void setRect(int x,int y,int w,int h);
        void setRect(SDL_Rect rect);
        void draw(void);
        void setX(int x);
        void setY(int y);
        void setXY(int x,int y);
        void setTextColor(int r,int g,int b,int a);
        void setColorMod(int r,int g,int b,int a);
        void setBackGroundColor(int r,int g,int b,int a);
        void setAlphaMod(int a);
        bool setFontSize(int fontSize);
        bool setFont(const char* font,int fontSize=24);
        void centerText(int w=SCREEN_W,int x=0);
        void centerText(const SDL_Rect& rect);
        #ifdef IMAGE_H
        void centerText(const Image& img);
        #endif // IMAGE_H
        void unSelect(void);

        //void changeTexture(const char *fichier,SDL_Renderer *render);
        void activateMouseOverEffect(void);
        void activateTextBackGround(bool state);
        void desactivateButtonMode(void){buttonMode=false;};

        int getX(void) const{return m_posText.x;};
        int getY(void) const{return m_posText.y;};
        int getRectW(void) const{return m_posText.w;};
        int getRectH(void) const{return m_posText.h;};
        SDL_Texture* getTexture(void) const{return m_text;};
        int getTextureW(void) const{return textureW;};
        int getTextureH(void) const{return textureH;};
        SDL_Rect getRect(void)const{return m_posText;};
        bool loadTextureFromText(std::string texte,int bg=-1);
        bool isMouseOn(void)const;
        bool isSelected(SDL_Event *ev);
        bool isInRect(int x,int y) const;
        void changeNumber(int a);




    private:
        SDL_Texture *m_text;
        TTF_Font *police;
        SDL_Color m_color;
        SDL_Color colorMod;
        SDL_Color bgColor;
        SDL_Rect m_posText;
        SDL_Renderer *m_renderer;
        int textureW;
        int textureH;
        std::string m_string;
        std::string fontString;
        int backGround;
        bool selectionState;
        bool buttonMode;
};

#endif // Text_H
