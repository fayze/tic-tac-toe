#include "CMain.h"
#include <iostream>

using std::cout;
using std::endl;

CMain::CMain(bool *isRunning,const char *name,int w,int h,int posX,int posY)
{
    *isRunning=true;
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

    if(!IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG |IMG_INIT_TIF | IMG_INIT_WEBP))
    {
        cout << "ERROR (initializing IMG library): "<<IMG_GetError()<<endl;
    }
    if(TTF_Init()<0)
    {
        cout<<"ERROR (initializing TTF library): "<<TTF_GetError()<<endl;
        *isRunning=false;
    }

    window = NULL;
    window=SDL_CreateWindow(name,posX,posY,w,h,SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    renderer=NULL;
    renderer=SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);
    if(window==NULL)
    {
        cout<<"Impossible de creer la fenetre";
        *isRunning=false;
    }

    SDL_Surface *icon=NULL;
    icon=IMG_Load("pics/logo.png");
    if(icon==NULL)
    {
        cout<<"ERROR (loading logo): "<<IMG_GetError()<<endl;
        *isRunning=false;
    }
    SDL_SetWindowIcon(window,icon);
    SDL_FreeSurface(icon);
    icon=NULL;
}

void CMain::Cbegin(void){
SDL_RenderClear(renderer);
}

void CMain::Cend(void){
SDL_RenderPresent(renderer);
}

CMain::~CMain()
{
    renderer=NULL;
    SDL_DestroyWindow(window);

    IMG_Quit();
    Mix_CloseAudio();
    TTF_Quit();
    SDL_Quit();
}
