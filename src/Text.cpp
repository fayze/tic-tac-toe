#include "Text.h"

using namespace std;

Text::Text(SDL_Renderer *renderer,bool *isRunning,std::string texte,int fontSize,int ActivateTextBackground)
{

    police=NULL;
    police=TTF_OpenFont(DEFAULTFONT.c_str(),fontSize);

    if(police==NULL)
    {
        cout<<"ERROR (loading font): "<<TTF_GetError()<<endl;
        *isRunning=false;
    }

    m_text=NULL;
    m_renderer=NULL;
    m_renderer=renderer;

    m_string=(texte!="")?texte:"Texte needs not to be empty!";
    fontString=DEFAULTFONT;

    bgColor={128,128,128,0};
    m_color={0,0,0,255};
    colorMod={0,185,185,0};
    backGround=ActivateTextBackground;
    selectionState=false;
    buttonMode=true;

    m_posText.x=0;
    m_posText.y=0;
    m_posText.w=0;
    m_posText.h=0;
    textureW=0;
    textureH=0;
    if(m_renderer==NULL)
    {
        cout <<"Impossible erreur de passage de renderer "<<SDL_GetError();
        *isRunning=false;
    }

    if(!Text::loadTextureFromText(m_string,backGround))
        *isRunning=false;

    SDL_QueryTexture(m_text,NULL,NULL,&textureW,&textureH);
    m_posText.w=textureW;
    m_posText.h=textureH;

    //ctor
}

bool Text::loadTextureFromText(std::string texte,int bg)
{

    m_string=texte;
    if(bg!=-1)
        backGround=bg;

    int w=0,h=0;

    if(m_string.length()==0)
        return false;
    else
    {
        if(backGround)
            m_text=SDL_CreateTextureFromSurface(m_renderer,TTF_RenderText_Shaded(police,m_string.c_str(),m_color,bgColor));
        else
            m_text=SDL_CreateTextureFromSurface(m_renderer,TTF_RenderText_Blended(police,m_string.c_str(),m_color));

        if(m_text==NULL)
            cout<<"Impossible de creer la texture ( "<<m_string<<") : "<<SDL_GetError()<<endl;
        else
        {
            SDL_QueryTexture(m_text,NULL,NULL,&w,&h);
            m_posText.w=(w < SCREEN_W)?w:SCREEN_W;
            m_posText.h=h;
        }
    }

    return true;
}


void Text::draw(void){
    SDL_RenderCopy(m_renderer,m_text,NULL,&m_posText);
}

void Text::setRect(int x,int y,int w,int h){
    m_posText.x=x;
    m_posText.y=y;
    m_posText.w=w;
    m_posText.h=h;
}

void Text::setRect(SDL_Rect rect){
    m_posText.x=rect.x;
    m_posText.y=rect.y;
    m_posText.w=rect.w;
    m_posText.h=rect.h;
}

void Text::setX(int x){
m_posText.x=x;
}


bool Text::isMouseOn(void) const
{
    bool state=false;
    int mouseX=0,mouseY=0;

    SDL_GetMouseState(&mouseX,&mouseY);

    if(buttonMode)
        if( (mouseX>=m_posText.x && mouseX<=m_posText.x+m_posText.w) &&
            (mouseY>=m_posText.y && mouseY<=m_posText.y+m_posText.h) && selectionState==false)
            state=true;

    return state;
}

void Text::activateMouseOverEffect(void)
{
    bool isIn=isMouseOn();

    if(isIn)
    {
        if(SDL_SetTextureColorMod(m_text,colorMod.r,colorMod.g,colorMod.b)<0)
            cout<<SDL_GetError()<<endl;
    }
    else
    {
        if(SDL_SetTextureColorMod(m_text,255,255,255)<0)
            cout<<SDL_GetError()<<endl;
    }
}

void Text::activateTextBackGround(bool state)
{
    backGround=state;
    Text::loadTextureFromText(m_string,backGround);
}

void Text::setY(int y){
m_posText.y=y;
}

void Text::setXY(int x,int y){
m_posText.x=x;
m_posText.y=y;
}

void Text::setTextColor(int r,int g,int b,int a)
{
    m_color.r=r;
    m_color.g=g;
    m_color.b=b;
    m_color.a=a;
    Text::loadTextureFromText(m_string,backGround);
}

void Text::setBackGroundColor(int r,int g,int b,int a)
{
    bgColor.r=r;
    bgColor.g=g;
    bgColor.b=b;
    bgColor.a=a;
    Text::loadTextureFromText(m_string,backGround);
}

void Text::setColorMod(int r,int g,int b,int a)
{
    colorMod.r=r;
    colorMod.g=g;
    colorMod.b=b;
    colorMod.a=a;
}

void Text::setAlphaMod(int a)
{
    a=(a>=0 && a<=255)?a:0;
    if(SDL_SetTextureAlphaMod(m_text,a)<0)
        cout<<"ERROR: "<<SDL_GetError()<<endl;
}

bool Text::setFontSize(int fontSize)
{
    bool allRight=true;
    police=NULL;
    police=TTF_OpenFont(fontString.c_str(),fontSize);

    if(police==NULL)
    {
        cout<<"ERROR (changing font size): "<<TTF_GetError()<<endl;
        cout<<"Using default font size: 24"<<endl;
        allRight=false;
        police=TTF_OpenFont(fontString.c_str(),24);
    }
    Text::loadTextureFromText(m_string);
    return allRight;
}

bool Text::setFont(const char* font,int fontsize)
{
    bool allRight=true;
    fontString=font;
    police=NULL;
    police=TTF_OpenFont(fontString.c_str(),fontsize);

    if(police==NULL)
    {
        cout<<"ERROR (changing font): "<<TTF_GetError()<<endl;
        cout<<"Using default font: sans_serif"<<endl;
        allRight=false;
        fontString=DEFAULTFONT;
        police=TTF_OpenFont(fontString.c_str(),fontsize);
    }

    return allRight;
}

void Text::centerText(int w,int x)
{
    if(w>m_posText.w)
    {
        m_posText.x=(w-m_posText.w)/2+x;
        cout<<"WESH"<<endl;
    }
    else
        m_posText.x=x;
}

void Text::centerText(const SDL_Rect& rect)
{
    if(rect.w>m_posText.w)
    {
        m_posText.x=(rect.w-m_posText.w)/2+rect.x;
        m_posText.y=rect.y;
    }
    else
        m_posText.x=rect.x;
}

#ifdef IMAGE_H
void Text::centerText(const Image& img)
{
    if(img.getRectW()>m_posText.w)
    {
        m_posText.x=(img.getRectW()-m_posText.w)/2+img.getX();
        m_posText.y=img.getY();
    }
    else
        m_posText.x=img.getX();
}
#endif // IMAGE_H
bool Text::isSelected(SDL_Event *ev)
{

    if(buttonMode)
    {
        if(Text::isMouseOn() && selectionState!=true)
        {
            if(ev->type==SDL_MOUSEBUTTONDOWN)
            {
                if(ev->button.button==SDL_BUTTON_LEFT)
                {
                    selectionState=true;
                    buttonMode=false;
                }
            }
        }
    }
    return selectionState;
}

void Text::unSelect(void)
{
    selectionState=false;
    buttonMode=true;
}

bool Text::isInRect(int mouseX,int mouseY) const
{
    bool  state=false;

    if( (mouseX>=m_posText.x && mouseX<=m_posText.x+m_posText.w) &&
            (mouseY>=m_posText.y && mouseY<=m_posText.y+m_posText.h) && selectionState==false)
            state=true;

    return state;
}

void Text::changeNumber(int a)
{
    std::string str="";
    str=to_string(a);
    loadTextureFromText(str.c_str(),-1);
}

Text::~Text(void)
{
    m_renderer=NULL;
    m_text=NULL;
    //dtor
}
