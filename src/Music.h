#ifndef MUSIC_H
#define MUSIC_H
#include "SDL_mixer.h"
#include <SDL_timer.h>


class Music
{
    public:
        Music(const char* filepath,bool *isRunning,bool isChunk=false,int frequency=44100,int channel=MIX_DEFAULT_CHANNELS,int chunksize=2048);
        ~Music(void);
        int playingMusic(void){return Mix_PlayingMusic();};
        int playMusic(int loops=0);
        int playChunk(int loops=0);
        int pausedMusic(void){return Mix_PausedMusic();};
        void resumeMusic(void);
        void pauseMusic(void);
        int haltMusic(void);
        int setMusicPosition(int seconds);
        int moveForward(int time=10);
        int moveBack(int time=10);
        void setJumpTime(int time=10);
        int getPosition(void);
        int changeMusic(const char *filepath);
        bool isFinished(void);
        unsigned lenght(void);

    private:
        Mix_Music *music;
        Mix_Chunk *chunk;
        int timer;
        int position;
        int ecart;
        int jumpSec;
        enum Action{STOP,PAUSE,PLAYING,FINISHED};
        Action action;
        unsigned taille;
        bool isChunK;

};

#endif // MUSIC_H
