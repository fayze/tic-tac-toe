#ifndef GAME_H
#define GAME_H
#include <vector>
#include <string>
#include <iostream>

#include "CMain.h"
#include "Image.h"
#include "Music.h"
#include "Text.h"
#include "Input.h"


class Game : public CMain
{
    public:
        Game();
        ~Game();
        enum class Etat {vide=0,cross,circle};

        void mainLoop(void);
        void draw(int a);
        void drawRectAt(int x,int y,bool vertical) const;
        void drawGrid(void) const;
        void drawResult(int a);
        bool proccessEvents(void);
        void fillTab(void);
        int logic(void);
        void reset(void);
        void restart(void);
        bool isNull(void);
        int checkGameState(void);


    protected:

    private:
        bool isRunning;
        bool action;
        int mouseX,mouseY;
        SDL_Event ev;

        Image *m_backGround;
        Image *m_cercle;
        Image *m_croix;
        Text *m_result;
        Text *m_reset;
        Text *m_scoreX;
        Text *m_scoreO;
        Music *m_sound;
        Input * m_input;


        std::vector<Etat> tab;
        Etat turn;

        int m_cercleInt;
        int m_croixInt;


       // enum




};

#endif // GAME_H
